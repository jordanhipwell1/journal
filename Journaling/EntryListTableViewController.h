//
//  EntryListTableViewController.h
//  Journaling
//
//  Created by Jordan Hipwell on 2/2/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EntryListTableViewController : UITableViewController

@end
