//
//  EntryListTableViewController.m
//  Journaling
//
//  Created by Jordan Hipwell on 2/2/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "EntryListTableViewController.h"
#import "NewEntryNavigationController.h"
#import "NewEntryViewController.h"
#import "AppDelegate.h"

@interface EntryListTableViewController ()

@property (nonatomic, weak) NSMutableArray *journalEntries;

@end

@implementation EntryListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set up navigation bar title label
    self.title = @"Journaling";
    
    UIFont *titleFont = [UIFont fontWithName:@"Journal" size:85];
    UIColor *titleColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    NSDictionary *titleAttributes = @{ NSFontAttributeName: titleFont,
                                       NSForegroundColorAttributeName: titleColor };
    self.navigationController.navigationBar.titleTextAttributes = titleAttributes;
    
    [self.navigationController.navigationBar setTitleVerticalPositionAdjustment:10 forBarMetrics:UIBarMetricsDefault];
    
    //set up right bar button item
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(showNewEntry:)];
    
    self.tableView.rowHeight = 75;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //enable interactive deselection via swipe back gesture
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    
    //get data from app delegate - table will load again
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.journalEntries = appDelegate.journalEntries; //point to same mutable array
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.journalEntries.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) { //haven’t created a cell with that id before
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.textLabel.text = self.journalEntries[indexPath.row][TitleIdentifier];
    cell.textLabel.font = [UIFont systemFontOfSize:20];
    cell.textLabel.textColor = [UIColor blackColor];
    
    NSDate *date = self.journalEntries[indexPath.row][DateIdentifier];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateStyle = NSDateFormatterMediumStyle;
    formatter.timeStyle = NSDateFormatterShortStyle;
    
    cell.detailTextLabel.text = [formatter stringFromDate:date];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
    cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    
    UIView *selectedView = [[UIView alloc] init];
    selectedView.backgroundColor = [UIColor colorWithWhite:245/255.0 alpha:1.0];
    cell.selectedBackgroundView = selectedView;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewEntryViewController *newEntryVC = [[NewEntryViewController alloc] init];
    
    newEntryVC.entryData = self.journalEntries[indexPath.row];
    newEntryVC.callback = ^(NSDictionary *modifiedEntry) {
        self.journalEntries[indexPath.row] = modifiedEntry;
        [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                              withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone]; //rehighlight modified row
    };
    
    [self.navigationController pushViewController:newEntryVC animated:YES];
}

#pragma mark - Methods

- (void)showNewEntry:(UIBarButtonItem *)sender {
    NewEntryNavigationController *newEntryNavVC = [[NewEntryNavigationController alloc] init];
    
    if ([newEntryNavVC.childViewControllers[0] isKindOfClass:[NewEntryViewController class]]) {
        NewEntryViewController *newEntryVC = (NewEntryViewController *)newEntryNavVC.viewControllers[0];
        newEntryVC.callback = ^(NSDictionary *newEntry) {
            [self.journalEntries insertObject:newEntry atIndex:0];
            //auto reloads on viewWillAppear
        };
    }
    
    [self.navigationController presentViewController:newEntryNavVC animated:YES completion:NULL];
}

@end
