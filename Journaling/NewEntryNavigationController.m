//
//  NewEntryNavigationController.m
//  Journaling
//
//  Created by Jordan Hipwell on 2/4/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "NewEntryNavigationController.h"
#import "NewEntryViewController.h"

@interface NewEntryNavigationController ()

@end

@implementation NewEntryNavigationController

- (instancetype)init {
    if (self = [super init]) {
        NewEntryViewController *vc = [[NewEntryViewController alloc] init];
        [self addChildViewController:vc];
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
