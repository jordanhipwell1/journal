//
//  NewEntryNavigationController.h
//  Journaling
//
//  Created by Jordan Hipwell on 2/4/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewEntryNavigationController : UINavigationController

@end
