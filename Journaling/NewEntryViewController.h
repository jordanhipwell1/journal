//
//  NewEntryViewController.h
//  Journaling
//
//  Created by Jordan Hipwell on 2/2/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewEntryViewController : UIViewController

@property (nonatomic, strong) NSDictionary *entryData;
@property (copy) void(^callback)(NSDictionary *);

@end
