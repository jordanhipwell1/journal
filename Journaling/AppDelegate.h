//
//  AppDelegate.h
//  Journaling
//
//  Created by Jordan Hipwell on 2/2/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableArray *journalEntries;

extern NSString * const DateIdentifier;
extern NSString * const TitleIdentifier;
extern NSString * const EntryIdentifier;

@end

