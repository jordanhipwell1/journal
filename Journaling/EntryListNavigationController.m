//
//  EntryListNavigationController.m
//  Journaling
//
//  Created by Jordan Hipwell on 2/4/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "EntryListNavigationController.h"
#import "EntryListTableViewController.h"

@interface EntryListNavigationController ()

@end

@implementation EntryListNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    EntryListTableViewController *vc = [[EntryListTableViewController alloc] init];
    [self addChildViewController:vc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
