//
//  NewEntryViewController.m
//  Journaling
//
//  Created by Jordan Hipwell on 2/2/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "NewEntryViewController.h"
#import "EntryListTableViewController.h"
#import "AppDelegate.h"

@interface NewEntryViewController () <UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, strong) UITextField *titleTextField;
@property (nonatomic, strong) UITextView *entryTextView;
@property (nonatomic, strong) UIBarButtonItem *saveButton;

@end

@implementation NewEntryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.edgesForExtendedLayout = UIRectEdgeNone; //move content under navigation bar
    
    CGFloat padding = 15;
    CGFloat verticalOffset = padding;
    
    //add text field for title
    self.titleTextField = [[UITextField alloc] initWithFrame:CGRectMake(padding, verticalOffset, self.view.frame.size.width - padding * 2, 30)];
    self.titleTextField.textAlignment = NSTextAlignmentCenter;
    self.titleTextField.placeholder = @"title";
    self.titleTextField.font = [[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline] fontWithSize:21];
    self.titleTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    self.titleTextField.returnKeyType = UIReturnKeyDone;
    self.titleTextField.delegate = self;
    [self.titleTextField addTarget:self
                       action:@selector(entryDataDidChange:)
             forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:self.titleTextField];
    
    verticalOffset += self.titleTextField.frame.size.height;
    verticalOffset += padding;
    
    //add separator view
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(self.titleTextField.frame.origin.x, verticalOffset, self.titleTextField.frame.size.width, 1)];
    separatorView.backgroundColor = [UIColor colorWithWhite:230/255.0 alpha:1.0];
    [self.view addSubview:separatorView];
    
    verticalOffset += 1;
    verticalOffset += padding;
    
    //add text view for entry
    self.entryTextView = [[UITextView alloc] initWithFrame:CGRectMake(self.titleTextField.frame.origin.x, verticalOffset, self.titleTextField.frame.size.width, self.view.frame.size.height - verticalOffset * 2)];
    self.entryTextView.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    self.entryTextView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive; //swipe down to dismiss
    self.entryTextView.alwaysBounceVertical = YES; //always allow scrolling vertically
    self.entryTextView.delegate = self;
    [self.view addSubview:self.entryTextView];
    
    //if data exists for entry, fill it in
    if (self.entryData) {
        self.titleTextField.text = self.entryData[TitleIdentifier];
        self.entryTextView.text = self.entryData[EntryIdentifier];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    //add bar button items if needed
    if ([self isModal]) {
        //cancel
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didTapCancel:)];
        //save
        self.saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(didTapSave:)];
        self.saveButton.enabled = NO;
        self.navigationItem.rightBarButtonItem = self.saveButton;
    }
    //only show back button for push
    else {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.callback) {
        if (self.entryData) { //don't call callback if user tapped cancel
            self.callback(@{DateIdentifier: [NSDate date],
                            TitleIdentifier: self.titleTextField.text,
                            EntryIdentifier: self.entryTextView.text});
        }
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //dismiss keyboard when user taps Done
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    [self entryDataDidChange:textView];
}

#pragma mark - Methods

- (void)didTapCancel:(UIBarButtonItem *)sender {
    [self.view endEditing:NO];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didTapSave:(UIBarButtonItem *)sender {
    [self.view endEditing:NO];
    
    self.entryData = @{DateIdentifier: [NSDate date],
                       TitleIdentifier: self.titleTextField.text,
                       EntryIdentifier: self.entryTextView.text};
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)entryDataDidChange:(id)sender {
    if ([self.titleTextField.text isEqualToString:@""]
        || [self.entryTextView.text isEqualToString:@""]) {
        self.saveButton.enabled = NO;
    } else {
        self.saveButton.enabled = YES;
    }
}

- (void)keyboardWillChangeFrame:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSInteger keyboardHeight = MIN(keyboardSize.height, keyboardSize.width);
    
    //move text up so it's all visible above keyboard
    self.entryTextView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0);
    self.entryTextView.scrollIndicatorInsets = self.entryTextView.contentInset;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.entryTextView.contentInset = UIEdgeInsetsZero;
    self.entryTextView.scrollIndicatorInsets = self.entryTextView.contentInset;
}

#pragma mark - Helper Methods

- (BOOL)isModal {
    if ([self presentingViewController])
        return YES;
    if ([[self presentingViewController] presentedViewController] == self)
        return YES;
    if ([[[self navigationController] presentingViewController] presentedViewController] == [self navigationController])
        return YES;
    if ([[[self tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]])
        return YES;
    
    return NO;
}

@end
